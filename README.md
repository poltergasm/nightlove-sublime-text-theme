![](screenshot.png)

## Nightlove - Sublime Text 2/3 theme

Just place the `tmTheme` file into your `Packages/User` directory and select the Nightlove color theme from Preferences -> Color theme
